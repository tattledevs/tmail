Reusable Email Template System for the Tattle Site emails that get sent out.

## Goals

 - Accept contact lists.
 - swappable templates (infinity+1 of them)
 - mockable send mechanism